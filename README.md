Resposive Web Design

El **diseño web adaptable** es una filosofía de diseño y desarrollo cuyo
objetivo es adaptar la apariencia de las páginas web al dispositivo que
se esté utilizando para visitarlas. Se pretende que con un único diseño
web, todo se vea correctamente en cualquier dispositivo o navegador.

([*https://es.wikipedia.org/wiki/Diseño\_web\_adaptable*](https://es.wikipedia.org/wiki/Diseño_web_adaptable))

Los media query es una técnica css introducida en css3. Usa la regla
@media para incluir bloques de propiedades css sólo si una cierta
condición se cumple.

([*https://www.w3schools.com/css/css\_rwd\_mediaqueries.asp*](https://www.w3schools.com/css/css_rwd_mediaqueries.asp),
[*https://developer.mozilla.org/es/docs/CSS/Media\_queries*](https://developer.mozilla.org/es/docs/CSS/Media_queries))

Diseño “Mobile First”

El diseño “Mobile First” significa diseñar para el móvil antes que para
escritorio o cualquier otro dispositivo, lo que hace que la página se
muestre más rápido en estos dispositivos. Esto requiere algunos cambios
en nuestro css. En lugar de cambiar los estilos cuando la anchura del
dispositivo es para móvil, se cambia cuando es mayor que para móvil.

Responsive Web Design – Grid System

¿Qué es un grid system?

Un sistema de grid es una estructura que permite que el contenido se
apile vertical y horizontalmente de una manera coherente y fácilmente
manejable (*al igual que haríamos con una tabla*). En definitiva es un
sistema de organización del contenido basado en filas y columnas.

Bootstrap

**Twitter** (empezó siendo una herramienta interna) **Bootstrap** es un
framework o conjunto de herramientas de código abierto para diseño de
sitios y aplicaciones web. Contiene plantillas de diseño con tipografía,
formularios, botones, cuadros, menús de navegación y otros elementos de
diseño basado en HTML y CSS, así como, extensiones de JavaScript
opcionales adicionales.

Se trata de dar consistencia al grueso de la aplicación para que tenga
el mismo aspecto.

Bootstrap tiene 4 major versión, nosotros trabajaremos con la versión 3.
La 4 está todavía en versión alpha y romperá la compatibilidad con las
versiones anteriores.

Este framework está pensado en el diseño del móvil primero, lo que
quiere decir que en caso de tener una disyuntiva elegirá primero el
diseño móvil (lo veremos en el grid)

Bootrap requiere **un elemento** contenedor para agrupar los contenidos
y albergar el grid. Hay que elegir entre el contenedor fijo por cada uno
de los dispositivos (container) y el fluido o adaptable
(container-fluid). Debido a los márgenes y algunas cosas más los
contenedores no son anidables.

Todos los elementos son clases y se insertan en el html a través del
atributo class=”container”. Dependiendo de las clases se pueden combinar
class=”btn btn-default”.

Bootstrap – Grid System

Los sistemas de grid en general se usan para crear layouts de filas y
columnas que albergan el contenido.

En particular, en bootstrap, esas filas tiene que estar contenidas
dentro de un container o container-fluid para que se ajusten
correctamente. Las filas se utilizan para crear grupos de columnas, el
contenido debe colocarse en las columnas y sólo las columnas pueden
anidar más filas con sus correspondientes columnas.

Bootstrap incluye un sistema de grid de 12 columnas fijo o fluido como
vimos anteriormente.

Si no se quieren las columnas individualmente se pueden agrupar para
crear columnas más amplias. Por ejemplo, en una pantalla grande el
contenido se organizará mejor en 3 columnas, pero en un móvil serían
demasiado pequeñas y habría que mostrarlas en 1 columna.

  Span1    Span1   Span1
  -------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- -------
  Span4    Span4   Span4
  Span4    Span8
  Span6    Span6
  Span12

Bootstrap Incluye clases predefinidas para construir la estructura
(layout) que nos interese. La herramienta tiene cuatro variaciones para
hacer uso de distintas resoluciones y tipos de dispositivos: teléfonos
móviles (**xs**), formato de retrato y paisaje (**sm**), tabletas y
computadoras con baja (**md**) y alta resolución (**lg**). Esto ajusta
el ancho de las columnas automáticamente a través de los media-querys.
Estos media-querys son los que dan la dimensión a cada uno de los
dispositivos.

Las columnas dejan espacio entre una y otra excepto la primera y la
última.

Si se especifican más de 12 columnas, las sobrantes se colocan como una
unidad en una fila nueva debajo.

Las clases del grid se aplican a dispositivos con anchos de pantalla
mayores o iguales a los tamaños del breakpoint (salto de dimensión) y
reemplazan las clases del grid dirigidas a dispositivos más pequeños.
Por ejemplo, aplicar cualquier clase .col-md- \* a un elemento no sólo
afectará su estilo en dispositivos medianos sino también en dispositivos
grandes si no existe una clase .col-lg- \*. Si no se ha especificado un
columna col-xs-\*, por defecto se aplica la col-xs-12 hasta la siguiente
clase si existe.

  Extra Small (**XS**)   Small (**SM**)   Medium (**MD**)   Large (**LG**)
  ---------------------- ---------------- ----------------- ----------------
  &lt;768px              &gt;=768px       &gt;=992px        &gt;=1200px

Una fila, al final, tiene que poder aplicar unas dimensiones a cada uno
de los tamaños de dispositivo (no olvidar el mobile first). Por defecto,
si no se especifica clase, se aplica el tamaño de su predecesor. El
tamaño por defecto es 12 si no hay predecesor.

Ejemplo:

**Class=””** o sin clases: al no tener clases, empezamos por el tamaño
del móvil (mobile first). El tamaño por defecto es 12, por lo que se le
aplica la clase col-xs-12. Después se analiza el siguiente tamaño del
dispositivo, al no existir ninguna clase y aplicar el tamaño de su
predecesor se le aplica el tamaño col-sm-12, y así sucesivamente.
Col-md-12 y col-lg-12. No poner clases sería equivalente a aplicar
class=”col-xs-12 col-sm-12 col-md-12 col-lg-12”

**XS – Extra pequeña (fila con 1 columna, clase no definida, agrupación
columna = 12)**

  -----------
  col-xs-12
  -----------

**SM - Pequeña (fila con 1 columna, clase no definida, agrupación
predecesor = 12)**

  -----------
  col-sm-12
  -----------

**MD – Media (fila con 1 columna, clase no definida, agrupación
predecesor = 12)**

  -----------
  col-md-12
  -----------

**LG - Grande (fila con 1 columna, clase no definida, agrupación
predecesor = 12)**

  -----------
  col-lg-12
  -----------

**Class=”col-md-6 col-lg-4”** -&gt; Como la columna XS no está definida,
por defecto es 12, col-xs-12. Para el tamaño SM, como no está definida
su clase, cogerá el tamaño de su predecesor, col-sm-12. Para el tamaño
MD, sí está definida su clase y aplicará col-md-6 y para el tamaño LG,
como también está definido se aplicará col-lg-4.

**XS – Extra pequeña (fila con 1 columna, clase no definida, agrupación
columna = 12)**

  -----------
  col-xs-12
  -----------

**SM – Pequeña (fila con 1 columna, clase no definida, agrupación
predecesor = 12)**

  -----------
  col-sm-12
  -----------

**MD – Media (fila con 2 columnas, clase definida, agrupación columna =
2)**

  ---------- ----------
  col-md-6   col-md-6
  ---------- ----------

**LG – Grande (fila con 3 columnas, clase definida, agrupación columna =
3)**

  ---------- ---------- ----------
  col-lg-4   col-lg-4   col-lg-4
  ---------- ---------- ----------

**Class=”col-sm-6 col-md-4”** -&gt; Columna XS no definida, col-xs-12.
Columna SM definida, col-sm-6. Columna MD definida, col-md-4. Columna LG
no definida, la de su predecesor col-lg-4.

**XS – Extra pequeña (fila con 1 columna, clase no definida, agrupación
columna = 12)**

  -----------
  col-xs-12
  -----------

**SM – Pequeña (fila con 2 columnas, clase definida, agrupación columna
= 6)**

  ---------- ----------
  col-sm-6   col-sm-6
  ---------- ----------

**MD – Media (fila con 3 columnas, clase definida, agrupación columna =
4)**

  ---------- ---------- ----------
  col-md-4   col-md-4   col-md-4
  ---------- ---------- ----------

**LG – Grande (fila con 3 columnas, clase no definida, tamaño predecesor
= 4)**

  ---------- ---------- ----------
  col-lg-4   col-lg-4   col-lg-4
  ---------- ---------- ----------

Cuando la suma de las columnas sobrepasa 12 unidades de la fila, pasan
abajo (como una unidad) a una nueva línea.

Offsetting columns

Se pueden mover columnas hacia la derecha usando col-xx-offset-\*. Estas
clases incrementan el margen izquierdo de una columna \* columnas. Por
ejemplo, col-md-offset-4 mueve col-md-4, cuatro columnas hacia la
derecha.

Las rejillas de Bootstrap tienen cuatro puntos de ruptura en los que las
columnas se reordenan, es casi seguro que te vas a encontrar con
problemas cuando las columnas tengan diferente altura. Para
solucionarlo, utiliza la clase .clearfix combinándola con alguna de las
clases auxiliares tipo .visible-xs

Bootstrap 3 introduce la posibilidad de reordenar las columnas para
cambiar su posición, lo que es muy importante para los diseños web
*responsive*. Añade las clases .col-md-push-\* y .col-md-pull-\* para
reordenar las columnas.
